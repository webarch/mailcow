# Webarchitects Mailcow Ansible Role

This repo previously contained [Ansible Playbooks for Mailcow Dockerized on Debian Stretch](https://git.coop/webarch/mailcow/-/tree/0.0.1) but it is now in the process of being refactored into a [Mailcow](https://mailcow.email/) Ansible role, but *it is at an alpha stage!*

## TODO:

* [Autodiscover / Autoconfig](https://mailcow.github.io/mailcow-dockerized-docs/u_e-autodiscover_config/)
* [SOGo Thunderbird connector](https://mailcow.github.io/mailcow-dockerized-docs/third_party-thunderbird/)

## Containers

List the containers:

```bash
cd /opt/mailcow-dockerized
docker-compose images
```

Show the state of the containers:

```bash
cd /opt/mailcow-dockerized
docker-compose ps
```

Stop all containers:

```bash
cd /opt/mailcow-dockerized
docker-compose down
```

Start all containers and detach (so you don't get the logs in the console):

```bash
cd /opt/mailcow-dockerized
docker-compose up -d
```

Restart a container:

```bash
cd /opt/mailcow-dockerized
docker-compose restart nginx-mailcow
```

Connect to a container, [see the documentation](https://mailcow.github.io/mailcow-dockerized-docs/debug-attach_service/), not all containers have Bash, some examples:

```bash
cd /opt/mailcow-dockerized
source mailcow.conf
docker-compose exec dovecot-mailcow /bin/sh
```

To connect to MySQL:

```bash
cd /opt/mailcow-dockerized
source mailcow.conf
docker-compose exec mysql-mailcow mysql -u${DBUSER} -p${DBPASS} ${DBNAME}
```

